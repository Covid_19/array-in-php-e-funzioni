<?php
$Auto = array("Lexus"=>"35.000", "Mercedes"=>"20.000", "Bmw"=>"10.000");
echo "Funzione asort() che ordina un array associativo per valori crescenti";
echo "<br>";
echo "<br>";

asort($Auto);
foreach($Auto as $x => $x_valore) {
    echo "Auto =  " . $x . ", Prezzo =  " . $x_valore;
    echo "<br>";
}
echo "<br>";
echo "Funzione ksort() che ordina un array associativo per il nome in ordine alfabetico";
echo "<br>";
echo "<br>";

ksort($Auto);
foreach($Auto as $x => $x_valore) {
    echo "Auto =  " . $x . ", Prezzo =  " . $x_valore;
    echo "<br>";
}

echo "<br>";
echo "Funzione arsort() che ordina un array associativo per il valore in ordine decrescente";
echo "<br>";
echo "<br>";

arsort($Auto);
foreach($Auto as $x => $x_valore) {
    echo "Auto =  " . $x . ", Prezzo =  " . $x_valore;
    echo "<br>";
}

echo "<br>";
echo "Funzione krsort() che ordina un array associativo per il nome in ordine alfabetico inverso";
echo "<br>";
echo "<br>";

krsort($Auto);
foreach($Auto as $x => $x_valore) {
    echo "Auto =  " . $x . ", Prezzo =  " . $x_valore;
    echo "<br>";
}


?>


